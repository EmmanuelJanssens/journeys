package com.journeys.main.model.projections;

import com.journeys.main.model.Coordinates;
import com.journeys.main.relationships.Experience;

import java.util.List;
import java.util.Set;

public interface JourneyExperiences {
    String getTitle();
    String getId();
    Set<Experiences> getExperiences();

    interface Experiences{
        String  getDescription();
        List<String> getImages();
        POIMarker getPointOfInterest();
    }
}
