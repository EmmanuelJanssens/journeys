package com.journeys.main.model.projections;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.journeys.main.model.Coordinates;
import com.journeys.main.model.PointOfInterest;

import java.time.LocalDateTime;
import java.util.List;

public interface ExperienceImagesDesc {
    int getOrder();
    String getDescription();
    List<String> getImages();
    POIIdCoordinates getPointOfInterest();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    LocalDateTime getDate();
    interface POIIdCoordinates{
        String getId();
        Coordinates getCoordinates();
    }
}
