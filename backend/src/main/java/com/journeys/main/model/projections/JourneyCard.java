package com.journeys.main.model.projections;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface JourneyCard {
    String getTitle();
    String getId();
    default List<String> getImages(){
        List<String> images = new ArrayList<>();
        for( ExperienceImages exp : getExperiences()){
            images.addAll(exp.getImages());
        }
        return images;
    }
    @JsonIgnore
    Set<ExperienceImages> getExperiences();
    interface ExperienceImages{
        List<String> getImages();
    }
}
