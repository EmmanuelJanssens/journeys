package com.journeys.main.model.projections;

import java.util.List;
import java.util.Set;

public interface POIExperiences {
    String getId();
    String getName();

    Set<JourneyNameId> getJourney();
    interface JourneyNameId{
        String getId();
        String getTitle();
        Set<JourneyExps> getExperiences();

    }

    interface JourneyExps{
        String getDescription();
        List<String> getImages();

        POIMarker getPointOfInterest();
    }
}
