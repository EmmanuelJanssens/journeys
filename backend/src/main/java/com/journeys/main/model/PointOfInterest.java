/**
 * @team Journeys
 * @file PointOfInterest.java
 * @date January 21st, 2022
 */

package com.journeys.main.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.neo4j.core.support.UUIDStringGenerator;

import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Node("POI")
public class PointOfInterest {
    @Id
    @GeneratedValue(generatorClass = UUIDStringGenerator.class)
    private String id;
    private String name;

    private String url;

    @Relationship(type = "LOCATED_AT")
    private Coordinates coordinates;

    @Nullable
    @Relationship(type = "IS_PART_OF")
    private Set<Journey> journey;

}