/**
 * @team Journeys
 * @file Journey.java
 * @date January 21st, 2022
 */

package com.journeys.main.model;

import com.journeys.main.relationships.Experience;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.neo4j.core.support.UUIDStringGenerator;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Node
public class Journey {
    @Id
    @GeneratedValue(generatorClass = UUIDStringGenerator.class)
    private String id;
    private String title;

    @Relationship(type = "START", direction = Relationship.Direction.OUTGOING)
    private Coordinates start;

    @Relationship(type = "END", direction = Relationship.Direction.OUTGOING)
    private Coordinates end;

    @Relationship(type = "EXPERIENCE", direction = Relationship.Direction.OUTGOING)
    private Set<Experience> experiences;
}
