package com.journeys.main.model.projections;

import java.util.Set;

public interface JourneyNameIdExperience {
    String getTitle();
    String getId();
    Set<ExperienceImagesDesc> getExperiences();
}
