/**
 * @team Journeys
 * @file UserJourneys.java
 * @date January 21st, 2022
 */

package com.journeys.main.model.projections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.journeys.main.relationships.Experience;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface UserJourneys {
    String getUserName();
    Set<JourneyCard> getJourneys();

}