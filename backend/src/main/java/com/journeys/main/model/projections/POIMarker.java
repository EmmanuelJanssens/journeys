package com.journeys.main.model.projections;

import com.journeys.main.model.Coordinates;

import java.util.Set;

public interface POIMarker {
    String getId();
    String getName();
    Coordinates getCoordinates();
}
