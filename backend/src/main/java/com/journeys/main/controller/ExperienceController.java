package com.journeys.main.controller;

import com.journeys.main.model.projections.ExperienceImagesDesc;
import com.journeys.main.relationships.Experience;
import com.journeys.main.service.AuthenticationService;
import com.journeys.main.service.ExperienceService;
import com.journeys.main.service.JourneyService;
import com.journeys.main.service.POIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/experience")
public class ExperienceController {

    private final POIService poiService;
    private final JourneyService journeyService;
    private final ExperienceService experienceService;
    private final AuthenticationService authenticationService;


    @Autowired
    public ExperienceController(POIService poiService, JourneyService journeyService, ExperienceService experienceService, AuthenticationService authenticationService) {
        this.poiService = poiService;
        this.journeyService = journeyService;
        this.experienceService = experienceService;
        this.authenticationService = authenticationService;
    }

    @GetMapping("{journey_id}/{poi_id}")
    public ExperienceImagesDesc getExperience(@PathVariable String journey_id, @PathVariable String poi_id){
       return experienceService.getExperience(journey_id,poi_id);
    }
}
