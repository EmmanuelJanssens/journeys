/**
 * @team Journeys
 * @file POIController.java
 * @date January 21st, 2022
 */

package com.journeys.main.controller;

import com.journeys.main.dto.POIFindBetween;
import com.journeys.main.model.Journey;
import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.projections.POIExperiences;
import com.journeys.main.model.projections.POIMarker;
import com.journeys.main.model.projections.POINameIdJourneys;
import com.journeys.main.relationships.Experience;
import com.journeys.main.service.JourneyService;
import com.journeys.main.service.POIService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("poi")
public class POIController {

    private final POIService poiService;
    private final JourneyService journeyService;

    /**
     * Constructor of POIController
     * @param poiService
     * @param journeyService
     */
    @Autowired
    public POIController(POIService poiService, JourneyService journeyService) {
        this.poiService = poiService;
        this.journeyService = journeyService;
    }

    /**
     * Get every point of interests
     * @return a collection of POI
     */
    @GetMapping
    public Page<POIMarker> getPOIs(@RequestParam int pageNb, @RequestParam int pageSize) {
        return poiService.getPOIs(PageRequest.of(pageNb,pageSize),POIMarker.class);
    }

    /**
     * Get a point of interest by its id
     * @param id the id of the POI
     * @return the object POI
     * @throws RuntimeException
     */
    @GetMapping("{id}")
    public POINameIdJourneys getPOI(@PathVariable("id") String id) throws RuntimeException {
        return poiService.getPOI(id,POINameIdJourneys.class);
    }

    @GetMapping("{id}/marker")
    public POIMarker getPOILocation(@PathVariable("id") String id) throws RuntimeException {
        return poiService.getPOI(id,POIMarker.class);
    }
    /**
     * Save a point of interest to the database
     * @param poi
     * @throws RuntimeException
     */
    @PostMapping
    public void savePOI(@RequestBody PointOfInterest poi) throws RuntimeException {
        poiService.savePOI(poi);
    }

    /**
     * Getting POI between two coordinates for preview
     * @param between a DTO object
     * @return a Collection of PoiPreview
     */
    @GetMapping(path = "/map")
    public Collection<POIMarker> getPOIMarkerLocAndID(POIFindBetween between) {
        return poiService.getMarkerLocAndID(between);
    }

    @GetMapping(path = "/preview")
    public Page<POINameIdJourneys> getPOIPreviewPaged(  POIFindBetween between,
                                                     @RequestParam int pageNb,
                                                     @RequestParam int pageSize) {
        return poiService.getPoiPreview(between,PageRequest.of(pageNb,pageSize));
    }


    //TODO move to dedicated experience controller
    @GetMapping("{id}/experience")
    public ResponseEntity<GetExperiences> getExperiences2(@PathVariable("id") String id) {

        PointOfInterest poi = poiService.getPoiExperiences(id,PointOfInterest.class);
        //journeys

        GetExperiences exps = new GetExperiences();
        exps.setName(poi.getName());
        exps.setId(poi.getId());
        exps.setExperiences(new HashSet<>());
        assert poi.getJourney() != null;
        for(Journey j : poi.getJourney() ){
            for(Experience e : j.getExperiences()){
                if(e.getPointOfInterest().getId().equals(id)){
                    System.out.println(e.getPointOfInterest().getId());
                    exps.getExperiences().add(new GetExperiences.Exp() {
                        @Override
                        public String getJourneyTitle() {
                            return j.getTitle();
                        }

                        @Override
                        public String getJourneyId() {
                            return j.getId();
                        }

                        @Override
                        public String getDescription() {
                            return e.getDescription();
                        }

                        @Override
                        public List<String> getImages() {
                            return e.getImages();
                        }
                    });
                }
            }
        }
        return ResponseEntity.ok().body(exps);
    }


    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    private static class GetExperiences{
        String name;
        String id;
        Set<Exp> experiences;

        private interface Exp{
            String getJourneyTitle();
            String getJourneyId();
            String getDescription();
            List<String> getImages();
        }
    }

}