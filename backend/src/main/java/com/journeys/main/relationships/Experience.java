/**
 * @team Journeys
 * @file Experience.java
 * @date January 21st, 2022
 */

package com.journeys.main.relationships;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.journeys.main.model.PointOfInterest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.driver.internal.shaded.reactor.util.annotation.Nullable;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@RelationshipProperties
public class Experience {
    @Id
    @GeneratedValue
    private Long id;
    private int order;

    @Nullable
    private String description;
    @Nullable
    private List<String> images;

    @TargetNode
    private PointOfInterest pointOfInterest;


    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDateTime  date;


}