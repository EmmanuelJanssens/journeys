package com.journeys.main.relationships;

import com.journeys.main.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RelationshipProperties
public class Friend {

    @Id
    @GeneratedValue
    private Long id;

    boolean accepted = false;

    @TargetNode
    User friend;

}
