package com.journeys.main.relationships;

import com.journeys.main.model.Notification;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RelationshipProperties
public class NotificationMessage {
    @Id
    @GeneratedValue
    Long id;

    private String message;
    private boolean read = false;
    private String targetUser;

    @TargetNode
    Notification notifications;
}
