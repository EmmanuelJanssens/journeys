/**
 * @team Journeys
 * @file POIRepository.java
 * @date January 21st, 2022
 */

package com.journeys.main.repositories;

import com.journeys.main.model.Coordinates;
import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.projections.POIMarker;
import com.journeys.main.model.projections.POINameIdJourneys;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface POIRepository extends Neo4jRepository<PointOfInterest, String> {


    <T> Optional<T> findById(String id, Class<T> type);
    <T> Optional<T> getPointOfInterestById(String id, Class<T> type);
    <T> Page<T>     findAllByIdIsNotNull(Pageable pageable, Class<T> type);
    <T> Page<T> getPointOfInterestByIdAndJourneyNotNull(String id, Pageable pageable, Class<T> type);

    @Query(value =
            "MATCH (p:POI) -[l:LOCATED_AT]-> (coordinates:Coordinates) \n" +
                    " WHERE \n" +
                    " coordinates.lat <= $fstLat AND \n" +
                    " coordinates.lng >= $fstLng AND \n" +
                    " coordinates.lat >= $sndLat AND \n" +
                    " coordinates.lng <= $sndLng \n" +
                    " RETURN p,l,coordinates ORDER BY p.name")
    Collection<POIMarker> getPoiMarkerLocAndID(
            Double fstLat,
            Double fstLng,
            Double sndLat,
            Double sndLng);

    @Query(value =
            "MATCH (p:POI) -[l:LOCATED_AT]-> (coordinates:Coordinates) \n" +
                    "OPTIONAL MATCH (p) -[i:IS_PART_OF]-(j) \n" +
                    "WITH j,i,p,l,coordinates\n" +
                    " WHERE \n" +
                    " coordinates.lat <= $fstLat AND \n" +
                    " coordinates.lng >= $fstLng AND \n" +
                    " coordinates.lat >= $sndLat AND \n" +
                    " coordinates.lng <= $sndLng \n" +
                    " RETURN collect(j),collect(i),p,l,coordinates ORDER BY p.name SKIP $skip LIMIT $limit",
    countQuery =
            "MATCH (p:POI) -[l:LOCATED_AT]-> (coordinates:Coordinates) \n" +
                    " WHERE \n" +
                    " coordinates.lat <= $fstLat AND \n" +
                    " coordinates.lng >= $fstLng AND \n" +
                    " coordinates.lat >= $sndLat AND \n" +
                    " coordinates.lng <= $sndLng \n" +
                    " RETURN COUNT(p)")
    Page<POINameIdJourneys> getPreviewPage(
            Double fstLat,
            Double fstLng,
            Double sndLat,
            Double sndLng, Pageable page);



}