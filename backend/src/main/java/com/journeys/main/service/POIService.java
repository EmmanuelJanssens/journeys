/**
 * @team Journeys
 * @file POIService.java
 * @date January 21st, 2022
 */

package com.journeys.main.service;

import com.journeys.main.dto.POIFindBetween;
import com.journeys.main.exceptions.POI.POINotFoundException;
import com.journeys.main.exceptions.POI.POIWriteFormatException;
import com.journeys.main.model.Coordinates;
import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.projections.POIMarker;
import com.journeys.main.model.projections.POINameIdJourneys;
import com.journeys.main.repositories.CoordinatesRepository;
import com.journeys.main.repositories.JourneyRepository;
import com.journeys.main.repositories.POIRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Transactional
@Service
public class POIService {

    private final double DELTA_LON = 0.05;
    private final double DELTA_LAT = DELTA_LON / 2;

    private final POIRepository poiRepository;
    private final JourneyRepository journeyRepository;
    private final CoordinatesRepository coordinatesRepository;

    /**
     * Constructor of POIService
     * @param poiRepository
     * @param journeyRepository
     * @param coordinatesRepository
     */
    @Autowired
    public POIService(POIRepository poiRepository, JourneyRepository journeyRepository, CoordinatesRepository coordinatesRepository) {
        this.poiRepository = poiRepository;
        this.journeyRepository = journeyRepository;
        this.coordinatesRepository = coordinatesRepository;
    }

    /**
     * Fetching every POI
     * @return a list of POI
     */
    public <T> Page<T> getPOIs(PageRequest pageRequest,Class<T> type) {
        return poiRepository.findAllByIdIsNotNull(pageRequest,type);
    }

    /**
     * Fetching a POI
     * @param id the id of the point of interest
     * @return the point of interest as an object
     * @throws POINotFoundException
     */
    public <T> T getPOI(String id,Class<T> type) throws POINotFoundException {
        Optional<T> result = poiRepository.findById(id,type);
        if (!result.isPresent())
            throw new POINotFoundException("Could not find POI " + id);
        return result.get();
    }

    public <T> T getPoiExperiences(String id,Class<T> type){
        return poiRepository.getPointOfInterestById(id,type).get();
    }
    /**
     * Saving a point of interest to database
     * @param poi a point of interest
     * @throws POIWriteFormatException
     */
    public void savePOI(PointOfInterest poi) throws POIWriteFormatException {
        poiRepository.save(poi);
    }

    public Collection<POIMarker> getMarkerLocAndID(POIFindBetween between){
        // Extending the top left corner more to the far left up
        Coordinates topLeft = new Coordinates();
        topLeft.setLat(Math.max(between.getStartLat(), between.getDestLat()) + DELTA_LAT);
        topLeft.setLng(Math.min(between.getStartLng(), between.getDestLng()) - DELTA_LON);

        // Extending the bottom right corner more to the far right down
        Coordinates bottomRight = new Coordinates();
        bottomRight.setLat(Math.min(between.getStartLat(), between.getDestLat()) - DELTA_LAT);
        bottomRight.setLng(Math.max(between.getStartLng(), between.getDestLng()) + DELTA_LON);


        return poiRepository.getPoiMarkerLocAndID(
                topLeft.getLat(),
                topLeft.getLng(),
                bottomRight.getLat(),
                bottomRight.getLng()
        );
    }

    public Page<POINameIdJourneys> getPoiPreview(POIFindBetween between,PageRequest pageRequest){
        // Extending the top left corner more to the far left up
        Coordinates topLeft = new Coordinates();
        topLeft.setLat(Math.max(between.getStartLat(), between.getDestLat()) + DELTA_LAT);
        topLeft.setLng(Math.min(between.getStartLng(), between.getDestLng()) - DELTA_LON);

        // Extending the bottom right corner more to the far right down
        Coordinates bottomRight = new Coordinates();
        bottomRight.setLat(Math.min(between.getStartLat(), between.getDestLat()) - DELTA_LAT);
        bottomRight.setLng(Math.max(between.getStartLng(), between.getDestLng()) + DELTA_LON);


        return poiRepository.getPreviewPage(
                topLeft.getLat(),
                topLeft.getLng(),
                bottomRight.getLat(),
                bottomRight.getLng(),
                pageRequest
        );
    }


    /**
     * Getting experiences from a journey
     * @param poi a point of interest
     * @return the list of JourneyExperiences
     */
    public <T> Page<T> getExperiencesBelongingToJourneys(String poi, Class<T> type) {
        return poiRepository.getPointOfInterestByIdAndJourneyNotNull(poi,PageRequest.of(0,100),type);
    }

}