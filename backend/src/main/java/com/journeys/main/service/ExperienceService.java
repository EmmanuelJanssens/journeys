package com.journeys.main.service;

import com.journeys.main.exceptions.experience.ExperienceNotFound;
import com.journeys.main.model.Coordinates;
import com.journeys.main.model.Journey;
import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.projections.ExperienceImagesDesc;
import com.journeys.main.relationships.Experience;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ExperienceService {

    private final JourneyService journeyService;
    private final POIService poiService;

    public ExperienceService(JourneyService journeyService, POIService poiService) {
        this.journeyService = journeyService;
        this.poiService = poiService;
    }

    public ExperienceImagesDesc getExperience(String journey_id, String poi_id) {
        Journey journey = journeyService.getJourney(journey_id,Journey.class);
        PointOfInterest poi = poiService.getPOI(poi_id,PointOfInterest.class);

        for(Experience e : journey.getExperiences()){
            if(e.getPointOfInterest().getId().equals(poi.getId())){
                return new ExperienceImagesDesc() {
                    @Override
                    public int getOrder() {
                        return e.getOrder();
                    }

                    @Override
                    public String getDescription() {
                        return e.getDescription();
                    }

                    @Override
                    public List<String> getImages() {
                        return e.getImages();
                    }

                    @Override
                    public POIIdCoordinates getPointOfInterest() {
                        return new POIIdCoordinates() {
                            @Override
                            public String getId() {
                                return poi.getId();
                            }

                            @Override
                            public Coordinates getCoordinates() {
                                return poi.getCoordinates();
                            }
                        };
                    }

                    @Override
                    public LocalDateTime getDate() {
                        return e.getDate();
                    }
                };
            }
        }
        throw new ExperienceNotFound("Could not find experience");
    }
}
